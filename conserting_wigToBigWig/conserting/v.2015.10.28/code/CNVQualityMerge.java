import java.io.*;
import java.util.*;
import java.text.*;

public class CNVQualityMerge {
    public static void main(String[] args) {
	CNVQualityMerge cnvq = new CNVQualityMerge();
	cnvq.process(args);
    }

    public void process (String[] args) {
	String segFile = null;
	String CRESTFile = "CREST_final_report.txt";
	String CRESTInput = null;
	String aiFile = null;
	String timestamp = "";
	boolean mm9 = false;
	double lpower = 3, gpower = 3, SV_minVal = 0.4, thresh_merging = 0.3, thresh_diff = 0.125, thresh_log = 0.170;
	int window = 100;
	int debug_chr = -1;
	boolean noSV = false;
	boolean debug = false, noMerge = false;
	SV[][] svs = new SV[25][10];
	int[] counts = new int[25];
	boolean male = false, readAI = false, checkDmeans = false;
	int[][] snps = new int[23][];
	    int ii = 0;
	    while (ii < args.length) {
		if (args[ii].compareToIgnoreCase("-seg") == 0) {// Segmentation file
		    ii++;
		    segFile = args[ii++];
		} else if (args[ii].compareToIgnoreCase("-c1") == 0) {// Orignial CREST file
		    ii++;
		    CRESTInput = args[ii++];
		} else if (args[ii].compareToIgnoreCase("-mm9") == 0) {// Mouse
		    ii++;
		    mm9 = true;
		    snps = new int[20][];
		} else if (args[ii].compareToIgnoreCase("-c2") == 0) {// Localized CREST result
		    ii++;
		    CRESTFile = args[ii++];
		} else if (args[ii].compareToIgnoreCase("-ai") == 0) {// LOH signal file
		    ii++;
		    aiFile = args[ii++];
		    readAI = true;
		} else if (args[ii].compareToIgnoreCase("-lr") == 0) {// LengthRatio power
		    ii++;
		    double d = Double.parseDouble(args[ii++]);
		    if (d > 1) lpower = d;
		} else if (args[ii].compareToIgnoreCase("-gm") == 0) {// GMean power
		    ii++;
		    double d = Double.parseDouble(args[ii++]);
		    if (d > 1) gpower = d;
		} else if (args[ii].compareToIgnoreCase("-svm") == 0) {// Default Value for no SV support
		    ii++;
		    SV_minVal = Double.parseDouble(args[ii++]);
		} else if (args[ii].compareToIgnoreCase("-male") == 0) { //Male
		    male = true;
		    ii++;
		} else if (args[ii].compareToIgnoreCase("-checkDmeans") == 0) { //CheckDmeans
		    checkDmeans = true;
		    ii++;
		} else if (args[ii].compareToIgnoreCase("-tm") == 0) { //Merge threshold
		    ii++;
		    double d = Double.parseDouble(args[ii++]);
		    if (d >= 0 && d < 1) thresh_merging = d;
		} else if (args[ii].compareToIgnoreCase("-td") == 0) { //Threshold for difference
		    ii++;
		    double d = Double.parseDouble(args[ii++]);
		    if (d >= 0 && d < 1) thresh_diff = d;
		} else if (args[ii].compareToIgnoreCase("-tl") == 0) { //Threshold for log2ratio
		    ii++;
		    double d = Double.parseDouble(args[ii++]);
		    if (d >= 0 && d < 1) thresh_log = d;
		} else if (args[ii].compareToIgnoreCase("-w") == 0) { //Window size
		    ii++;
		    int d = Integer.parseInt(args[ii++]);
		    if (d >= 0) window = d;
		} else if (args[ii].compareToIgnoreCase("-ts") == 0) { //Optinal time stamp
		    ii++;
		    timestamp = args[ii++];
		} else if (args[ii].compareToIgnoreCase("-debug") == 0) {
		    debug = true;
		    ii++;
		} else if (args[ii].compareToIgnoreCase("-noMerge") == 0) {
		    noMerge = true;
		    ii++;
		} else if (args[ii].compareToIgnoreCase("-debugChr") == 0) {
		    ii++;
		    debug_chr = Integer.parseInt(args[ii]);
		    ii++;
		}
	    }
	System.out.println("AIFile: " + aiFile);
	System.out.println("SegFile: " + segFile);
	if (debug) System.out.println("Chr for debugging:\t" + debug_chr);

	double distance_thresh = 5 * window;
	double[][] svScores = new double[3][3];
	svScores[2][2] = 1; svScores[0][0] = SV_minVal;
	svScores[0][1] = 0.375 * (1 - SV_minVal) + SV_minVal; svScores[1][0] = svScores[0][1];
	svScores[0][2] = 0.5 + 0.5 * SV_minVal; svScores[2][0] = svScores[0][2];
	svScores[1][2] = 0.875 * (1 - SV_minVal) + SV_minVal; svScores[2][1] = svScores[1][2];
//	svScores[1][2] = 1; svScores[2][1] = svScores[1][2];
	svScores[1][1] = 0.75 * (1 - SV_minVal) + SV_minVal;
	if (CRESTInput == null) noSV = true;
	if (segFile == null) {
	    System.err.println("No CONSERTING output files.");
	    System.exit(-1);
	}
	try {
	    if (readAI) {
		System.out.println("Loading AI input: " + aiFile);
		BufferedReader br = new BufferedReader(new FileReader(aiFile));
		String line = br.readLine();
		String currentChr = null;
		int count = 0, chr = -1;
		int[] itmp = null;
		while ((line = br.readLine()) != null) {
		    StringTokenizer st = new StringTokenizer(line, "\t");
		    String schr = st.nextToken();
		    if (currentChr == null) {
			itmp = new int[1000];
			chr = Integer.parseInt(schr);
			currentChr = schr;
		    } else if (currentChr.compareTo(schr) != 0) {
			if (chr <= snps.length) {
			    snps[chr - 1] = new int[count];
			    System.arraycopy(itmp, 0, snps[chr - 1], 0, count);
//			    System.out.println("Chromosomes " + chr + ":\t" + count + " SNPs");
			}
			count = 0;
			currentChr = schr;
			try {
			    chr = Integer.parseInt(schr);
			} catch (NumberFormatException nfe) {
			    chr = (schr.compareTo("X") == 0 || schr.compareTo("chrX") == 0) ? 23 : 24;
			    if (mm9) chr = (schr.compareTo("X") == 0 || schr.compareTo("chrX") == 0) ? 20 : 21;
			}
		    }
		    if (chr > snps.length) continue;
		    if (count == itmp.length) {	
			int[] iitmp = new int[2 * count];
			System.arraycopy(itmp, 0, iitmp, 0, count);
			itmp = iitmp;
		    }
		    itmp[count++] = Integer.parseInt(st.nextToken());
		}
		if (chr < snps.length) {
		    snps[chr - 1] = new int[count];
		    System.arraycopy(itmp, 0, snps[chr - 1], 0, count);
		}
		br.close();
	    }
	} catch (IOException ie) {
	    readAI = false;
	}
	try {
	    if (!noSV) {
		System.out.println("Loading original CREST input: " + CRESTInput);
		BufferedReader br = new BufferedReader(new FileReader(CRESTInput));
		String line = null;
		while ((line = br.readLine()) != null) {
		    if (line.indexOf("chrA") >= 0) continue;
		    StringTokenizer st = new StringTokenizer(line, "\t");
		    boolean readCounts = (st.countTokens() > 6);
		    String stmp = null;
		    try {
			    stmp = st.nextToken();
		    } catch (Exception ee) {
			System.out.println();
			ee.printStackTrace();
			System.exit(-4);
		    }
		    int chrA = 0;
		    try {
			chrA = Integer.parseInt(stmp);
		    } catch (NumberFormatException nfe) {
			if (stmp.compareTo("X") == 0) {
			    chrA = 23;
			    if (mm9) chrA = 20;
			}
			if (stmp.compareTo("Y") == 0) {
			    chrA = 24;
			    if (mm9) chrA = 21;
			}
		    }
		    int posA = Integer.parseInt(st.nextToken());
		    boolean oriA = st.nextToken().startsWith("+");
		    int countA = 2, countB = 2;
		    if (readCounts) countA = Integer.parseInt(st.nextToken());
		    stmp = st.nextToken();
		    int chrB = 0;
		    try {
			chrB = Integer.parseInt(stmp);
		    } catch (NumberFormatException nfe) {
			if (stmp.compareTo("X") == 0) {
			    chrB = 23;
			    if (mm9) chrB = 20;
			}
			if (stmp.compareTo("Y") == 0) {
			    chrB = 24;
			    if (mm9) chrB = 21;
			}
		    }
		    int posB = Integer.parseInt(st.nextToken());
		    boolean oriB = st.nextToken().startsWith("+");
		    if (readCounts) countB = Integer.parseInt(st.nextToken());
		    SV s = new SV(chrA, posA, oriA, countA, chrB, posB, oriB, countB);
		    if (counts[chrA] == svs[chrA].length) {
			SV[] svtmp = new SV[2 * counts[chrA]];
			System.arraycopy(svs[chrA], 0, svtmp, 0, counts[chrA]);
			svs[chrA] = svtmp;
		    }
		    svs[chrA][counts[chrA]++] = s;
		    if (counts[chrB] == svs[chrB].length) {
			SV[] svtmp = new SV[2 * counts[chrB]];
			System.arraycopy(svs[chrB], 0, svtmp, 0, counts[chrB]);
			svs[chrB] = svtmp;
		    }
		    svs[chrB][counts[chrB]++] = s;
		}
		br.close();
		System.out.println("Loading local CREST input: " + CRESTFile);
		br = new BufferedReader(new FileReader(CRESTFile));
		while ((line = br.readLine()) != null) {
		    StringTokenizer st = new StringTokenizer(line, "\t");
		    String stmp = st.nextToken();
		    int chrA = 0;
		    try {
			chrA = Integer.parseInt(stmp);
		    } catch (NumberFormatException nfe) {
			if (stmp.compareTo("X") == 0) {
			    chrA = 23;
			    if (mm9) chrA = 20;
			}
			if (stmp.compareTo("Y") == 0) {
			    chrA = 24;
			    if (mm9) chrA = 21;
			}
		    }
		    int posA = Integer.parseInt(st.nextToken());
		    boolean oriA = st.nextToken().startsWith("+");
		    int countA = Integer.parseInt(st.nextToken());
		    stmp = st.nextToken();
		    int chrB = 0;
		    try {
			chrB = Integer.parseInt(stmp);
		    } catch (NumberFormatException nfe) {
			if (stmp.compareTo("X") == 0) {
			    chrB = 23;
			    if (mm9) chrB = 20;
			}
			if (stmp.compareTo("Y") == 0) {
			    chrB = 24;
			    if (mm9) chrB = 21;
			}
		    }
		    int posB = Integer.parseInt(st.nextToken());
		    boolean oriB = st.nextToken().startsWith("+");
		    int countB = Integer.parseInt(st.nextToken());
		    SV s = new SV(chrA, posA, oriA, countA, chrB, posB, oriB, countB);
		    boolean match = false;
		    for (int i = 0; i < counts[chrA]; i++) {
			if (s.isSame(svs[chrA][i])) {
			    if (s.oriA == svs[chrA][i].oriA) {
				if (s.countA > svs[chrA][i].countA) svs[chrA][i].countA = s.countA;
				if (s.countB > svs[chrA][i].countB) svs[chrA][i].countB = s.countB;
			    } else {
				if (s.countA > svs[chrA][i].countB) svs[chrA][i].countB = s.countA;
				if (s.countB > svs[chrA][i].countA) svs[chrA][i].countA = s.countB;
			    }
			    match = true;
			    break;
			}
		    }
		    if (match) continue;
		    if (counts[chrA] == svs[chrA].length) {
			SV[] svtmp = new SV[2 * counts[chrA]];
			System.arraycopy(svs[chrA], 0, svtmp, 0, counts[chrA]);
			svs[chrA] = svtmp;
		    }
		    svs[chrA][counts[chrA]++] = s;
		    if (counts[chrB] == svs[chrB].length) {
			SV[] svtmp = new SV[2 * counts[chrB]];
			System.arraycopy(svs[chrB], 0, svtmp, 0, counts[chrB]);
			svs[chrB] = svtmp;
		    }
		    svs[chrB][counts[chrB]++] = s;
		}
		br.close();
	    }
	    BufferedReader br = new BufferedReader(new FileReader(segFile));
	    double ploidy = 0;
	    double totalBin = 0, currentNum = 0;
	    String line = br.readLine();
	    PrintWriter pw = new PrintWriter(new FileWriter(segFile + (noMerge ? ".QualityMerge.init" : ".QualityMerge"))),
		pw2 = new PrintWriter(new FileWriter(segFile + timestamp + ".QualityMerge"));
	    pw.println(line + "\tQualityScore\tSV_Matched");
	    pw2.println(line + "\tQualityScore\tSV_Matched");
	    double currentLscore = 0, currentGscore = 0, lscore = 0, gscore = 0, svscore = 1, currentVscore = 0, currentDmeanscore = 0, currentG = 0;
	    int[][] segI = new int[200][5];
	    double[][] segD = new double[segI.length][7];
	    int count = 0;
	    while ((line = br.readLine()) != null) {
		StringTokenizer st = new StringTokenizer(line, "\t");
		int chr = Integer.parseInt(st.nextToken()), posA = Integer.parseInt(st.nextToken());
		int posB = 0;
		int currentB = 0;
		String ss = st.nextToken();
		double num = Double.parseDouble(st.nextToken());
		if (chr < snps.length) totalBin += num;
		try {
		    posB = Integer.parseInt(ss);
		} catch (NumberFormatException nfe) {
		    posB = Math.round(Float.parseFloat(ss));
		}
		double lr = Double.parseDouble(st.nextToken());
		if (lr <= 0.15 || (lr <= 0.25 && num <1000)) continue;	
		if (lr > 0.9 || (num > 10000 && lr > 0.475) || num > 100000) {
		    lscore = 1;
		} else {
		    lscore = Math.pow(lr/0.9, lpower);
		}
		double diff = Double.parseDouble(st.nextToken());
		double gmean = Double.parseDouble(st.nextToken());
		if (gmean >= 2.5) continue;
		double dmean = Double.parseDouble(st.nextToken());
		double logratio = Double.parseDouble(st.nextToken());
		if (chr < snps.length) ploidy += (num * logratio);
		double vscore = ((Math.abs(diff) < thresh_diff && Math.abs(logratio) < thresh_log) || (diff * logratio > 0)) ? 1 : 0;
		double g = (male && chr >= snps.length) ? (gmean * 2) : gmean, dd = (male && chr >= snps.length) ? (dmean * 2) : dmean;
		gscore = (g > 2) ? (-1) : Math.pow((1 - Math.abs(g - 1))/0.9, gpower);
		double dmeanscore = (num >= 10000 || Math.abs(dmean - 1) >= thresh_diff || (Math.abs(diff) < thresh_diff && Math.abs(logratio) < thresh_log) || (diff * logratio <= 0)) ? 1 : Math.pow(Math.abs(dmean - 1)/thresh_diff, gpower);
		int sv_match = 0;
		if (readAI && g < 0.9 && (chr < snps.length || (!male && chr == snps.length))) {
		    int c1 = -1;
		    try {
			Arrays.binarySearch(snps[chr - 1], posA);
		    } catch (Exception ee) {
			System.err.println("male = " + male + "\t" + snps.length + "\t" + chr + "\t" + posB);
			ee.printStackTrace();
			System.exit(-1);
		    }
		    int c2 = Arrays.binarySearch(snps[chr - 1], posB);
		    if (c1 < 0) c1 = -c1 - 1;
		    if (c2 < 0) c2 = -c2 - 1;
		    double g1 = (c2 - c1) * (snps[chr - 1][snps[chr - 1].length - 1] - snps[chr - 1][0]) * 1.0 /(snps[chr - 1].length * (posB - posA + 1));
		    if (g1 > gscore) gscore = g1;
		}
		if (gscore > 1) gscore = 1;
		double score = 0;
		if (count == 0 || chr != segI[count - 1][0]) {
		    score = 1;
		    sv_match = 1;
		} else {
		    int oldPosA = posA;
		    int dist = posA - segI[count - 1][2];
		    if (!noSV) {
			double dist_thresh = (num * window / 2 < distance_thresh) ? num * window / 2 : distance_thresh;
			svscore = SV_minVal;
			double internal = 1.5;
			for (int i = 0; i < counts[chr]; i++) {
			    if (svs[chr][i].chrA == chr && (Math.abs(svs[chr][i].posA - oldPosA) <= dist_thresh || (oldPosA - svs[chr][i].posA <= dist && oldPosA - svs[chr][i].posA > 0)) && 
				(svs[chr][i].chrB != chr || Math.abs(svs[chr][i].posB - oldPosA) >= Math.abs(svs[chr][i].posA - oldPosA)) &&
				((diff > segD[count - 1][1] && (!svs[chr][i].oriA)) || (diff < segD[count - 1][1] && (svs[chr][i].oriA)))) {
				//svs[chr][i].confirmA = true;
				int c1 = (svs[chr][i].countA > 1) ? 2 : svs[chr][i].countA;
				int c2 = (svs[chr][i].countB > 1) ? 2 : svs[chr][i].countB;
				if (svScores[c1][c2] > svscore) {
				    svscore = svScores[c1][c2];
				}
				sv_match = 1;
				if (Math.abs(svs[chr][i].posA - oldPosA) < window / 2) {
				    double valtmp = (svs[chr][i].countA + 0.5) * (svs[chr][i].countB + 0.5);
				    if (valtmp > internal) { 
					internal = valtmp;
					posA = svs[chr][i].posA;
				    }
				}
			    }
			    if (svs[chr][i].chrB == chr && (Math.abs(svs[chr][i].posB - oldPosA) <= dist_thresh || (oldPosA - svs[chr][i].posB <= dist && oldPosA - svs[chr][i].posB > 0)) && 
				(svs[chr][i].chrA != chr || Math.abs(svs[chr][i].posA - oldPosA) >= Math.abs(svs[chr][i].posB - oldPosA)) &&
				((diff < segD[count - 1][1] && (!svs[chr][i].oriB)) || (diff > segD[count - 1][1] && (svs[chr][i].oriB)))) {
				//svs[chr][i].confirmB = true;
				int c1 = (svs[chr][i].countA > 1) ? 2 : svs[chr][i].countA;
				int c2 = (svs[chr][i].countB > 1) ? 2 : svs[chr][i].countB;
				if (svScores[c1][c2] > svscore) {
				    svscore = svScores[c1][c2];
				}
				sv_match = 1;
				if (Math.abs(svs[chr][i].posB - oldPosA) < window / 2) {
				    double valtmp = (svs[chr][i].countA + 0.5) * (svs[chr][i].countB + 0.5);
				    if (valtmp > internal) { 
					internal = valtmp;
					posA = svs[chr][i].posB;
				    }
				}
			    }
			}
		    }
		    if (currentNum >= 10000 && num >= 10000) svscore = Math.max(svscore, svScores[0][1]);
		    double dscore = Math.abs(diff - segD[count - 1][1]) / thresh_diff;
		    if (chr >= snps.length && male) dscore *= 2;
		    if (dscore > 8) {
			dscore = 2;
		    } else if (dscore > 1) {
			dscore = 1 + (dscore - 1) / 7;
		    }
		    double lrscore = Math.abs(logratio - segD[count - 1][4]) / thresh_log;
		    if (lrscore > 6) {
			lrscore = 2;
		    } else if (lrscore > 1) {
			lrscore = 1 + (lrscore - 1) / 5;
		    }
		    score = svscore * lscore * gscore * currentLscore * currentGscore * vscore * currentVscore * lrscore * dscore;
		    if (posA - currentB >= 100 * window) score = Math.max(svscore, svScores[0][1]) * gscore * currentGscore * vscore * currentVscore * lrscore * dscore;
		    if (checkDmeans) score *= (dmeanscore * currentDmeanscore);
		    if (svscore >=  svScores[2][0] && score < svscore * vscore * currentVscore * lrscore * dscore) score = svscore * lrscore * dscore;
		    if ((num <= 50 || currentNum < 50 || (num <= 1000 && Math.abs(g - 1) > 0.2) || (currentNum <= 1000 && Math.abs(currentG - 1) > 0.2)) && sv_match == 0) score /= 10;
		    if (oldPosA - segI[count - 1][2] < window) {
			segI[count - 1][2] = posA - 1;
		    }
		    //pw.println("\t" + score);
		    if (debug && debug_chr == chr) System.out.println(chr + " " + posA + " " + posB + " " + score + " " + svscore + " " + lscore + " " + gscore + " " + vscore + " " + lrscore + " " + dscore);
		}
		if (count == segI.length) {
		    int[][] itmp = new int[2 * count][segI[0].length];
		    System.arraycopy(segI, 0, itmp, 0, count);
		    segI = itmp;
		    double[][] dtmp = new double[2 * count][segD[0].length];
		    System.arraycopy(segD, 0, dtmp, 0, count);
		    segD = dtmp;
		}
		segI[count][0] =chr;
		segI[count][1] = posA;
		segI[count][2] = posB;
		segI[count][3] = (int) Math.round(num);
		segI[count][4] = sv_match;
		segD[count][0] = lr;
		segD[count][1] = diff;
		segD[count][2] = gmean;
		segD[count][3] = dmean;
		segD[count][4] = logratio;
		segD[count][5] = score;
		segD[count][6] = svscore;
		if (score > 0) {
		    currentLscore = lscore;
		    currentGscore = gscore;
		    currentG = g;
		    currentVscore = vscore;
		    currentDmeanscore = dmeanscore;
		    currentNum = num;
		    currentB = posB;
		}
		count++;
	    }
	    br.close();
//	    for (int i = 0; i < count; i++)
//		System.out.println(segI[i][0] + "\t" + segI[i][1] + "\t" + segD[i][5] + "\t" + segD[i][6]);
	    ploidy /= totalBin;
	    if (Math.abs(ploidy) > 0.1) thresh_merging *= ((1 + 5 * Math.abs(ploidy)) / 1.5);
	    if (thresh_merging > 0.48) thresh_merging = 0.48;
	    System.out.println("Average log2 ratio on autosomes:\t" + ploidy);
//	    System.out.println("Entries read: " + count);
	    for (int i = count; i < segD.length; i++) segD[i] = null;
	    while (!noMerge) {
		double min_score = 1, min_diff = 1;
		int min_idx = -1;
		for (int i = 0; i < count; i++) {
		    if (segD[i][5] < min_score) {
			min_score = segD[i][5];
			min_diff = Math.abs(segD[i][1] - segD[i - 1][1]);
			min_idx = i;
		    } else if (i > 0 && segD[i][5] == min_score && Math.abs(segD[i][1] - segD[i - 1][1]) < min_diff) {
			min_diff = Math.abs(segD[i][1] - segD[i - 1][1]);
			min_idx = i;
		    }
		}
		if (debug) {
		    if (debug_chr < 0 || segI[min_idx-1][0] == debug_chr) {
			System.out.println();
			System.out.println(segI[min_idx-1][0] + " " + segI[min_idx-1][1] + " " + segI[min_idx-1][2] + " " + segI[min_idx-1][3] + " " + segD[min_idx-1][0] + " " + segD[min_idx-1][1] + " " + segD[min_idx-1][2] + " " + segD[min_idx-1][3] + " " + segD[min_idx-1][4] + " " + segD[min_idx-1][5] + " " + segI[min_idx-1][4]);
			System.out.println(segI[min_idx][0] + " " + segI[min_idx][1] + " " + segI[min_idx][2] + " " + segI[min_idx][3] + " " + segD[min_idx][0] + " " + segD[min_idx][1] + " " + segD[min_idx][2] + " " + segD[min_idx][3] + " " + segD[min_idx][4] + " " + segD[min_idx][5] + " " + segI[min_idx][4]);
		    }
		}
		//System.out.println(min_idx + " " + min_diff + " " + min_score + " " + count + "\t");
		if (min_score < thresh_merging) {
		    double diff1 =  segD[min_idx - 1][1], diff2 = segD[min_idx][1];
		    if ((Math.min(segI[min_idx - 1][3], segI[min_idx][3]) * 20 * window < segI[min_idx][1] - segI[min_idx - 1][2]) || (min_score < 0.001 && segD[min_idx - 1][1] == segD[min_idx][1] && segD[min_idx-1][4] == segD[min_idx][4] && (segI[min_idx][1]-segI[min_idx-1][2] > segI[min_idx - 1][3]*window*5 || segI[min_idx][1]-segI[min_idx-1][2] > segI[min_idx][3]*window*5) && (segI[min_idx - 1][3]/segI[min_idx][3]>=9 || segI[min_idx][3]/segI[min_idx-1][3]>=9))) {
			if (segI[min_idx - 1][3] < segI[min_idx][3]) {
			    segD[min_idx - 1] = segD[min_idx];
			    segI[min_idx - 1] = segI[min_idx];
			}
			if (debug && (debug_chr < 0 || segI[min_idx-1][0] == debug_chr)) System.out.println(segI[min_idx-1][0] + "\t" + segI[min_idx-1][1] + "\t" + segI[min_idx-1][2] + "\t" + segD[min_idx-1][0]);
		    } else {
			if (min_score < 0.001 && (segI[min_idx][4] == 1 || segI[min_idx][4] == 3) && min_idx + 1 < count && segI[min_idx][0]==segI[min_idx+1][0] && (segI[min_idx+1][4] == 0 || segI[min_idx+1][4] == 2)) min_idx++;
			segD[min_idx - 1][1] = (segD[min_idx - 1][1] * segI[min_idx - 1][3] + segD[min_idx][1] * segI[min_idx][3]) / (segI[min_idx - 1][3] + segI[min_idx][3]);
			segD[min_idx - 1][2] = (segD[min_idx - 1][2] * segI[min_idx - 1][3] + segD[min_idx][2] * segI[min_idx][3]) / (segI[min_idx - 1][3] + segI[min_idx][3]);
			segD[min_idx - 1][3] = (segD[min_idx - 1][3] * segI[min_idx - 1][3] + segD[min_idx][3] * segI[min_idx][3]) / (segI[min_idx - 1][3] + segI[min_idx][3]);
			segD[min_idx - 1][4] = (segD[min_idx - 1][4] * segI[min_idx - 1][3] + segD[min_idx][4] * segI[min_idx][3]) / (segI[min_idx - 1][3] + segI[min_idx][3]);
			segI[min_idx - 1][2] = segI[min_idx][2];
			segI[min_idx - 1][3] += segI[min_idx][3];
			segD[min_idx - 1][0] = (segI[min_idx - 1][3] * 100.0) / (segI[min_idx - 1][2] - segI[min_idx - 1][1] + 1);
		    }

		    if (min_idx < count - 1) {
//			System.out.println("Removed Entries: " + segI[min_idx][0] + " " + segI[min_idx][1] + " " + segI[min_idx][2]);
			System.arraycopy(segI, min_idx + 1, segI, min_idx, count - min_idx - 1);
			System.arraycopy(segD, min_idx + 1, segD, min_idx, count - min_idx - 1);
		    }
		    segI[count - 1] = null;
		    segD[count - 1] = null;
		    count--;
		    if (min_idx == 1 || segI[min_idx - 2][0] != segI[min_idx - 1][0]) {
			segD[min_idx - 1][5] = 1;
		    } else {
			if ((segD[min_idx - 2][1] - diff1) * (segD[min_idx - 2][1] - segD[min_idx - 1][1]) < 0 && (!noSV)) {
			    int dist = segI[min_idx-1][1]-segI[min_idx-2][2];
			    int oldPosA = (int) (Math.round((segI[min_idx - 1][1] - 1) / 100.0) * 100) + 1, posA = oldPosA, sv_match=0, chr = segI[min_idx - 1][0];
			    int num = segI[min_idx - 1][3];
			    double dist_thresh = (num * window / 2 < distance_thresh) ? num * window / 2 : distance_thresh;
			    svscore = SV_minVal;
			    double internal = 1.5;
			    for (int i = 0; i < counts[chr]; i++) {
			    	if (svs[chr][i].chrA == chr && (Math.abs(svs[chr][i].posA - oldPosA) <= dist_thresh || (oldPosA - svs[chr][i].posA < dist && oldPosA - svs[chr][i].posA > 0)) && ((segD[min_idx - 1][1] > segD[min_idx - 2][1] && (!svs[chr][i].oriA)) || (segD[min_idx - 1][1] < segD[min_idx - 2][1] && (svs[chr][i].oriA)))) {
				    int c1 = (svs[chr][i].countA > 1) ? 2 : svs[chr][i].countA;
				    int c2 = (svs[chr][i].countB > 1) ? 2 : svs[chr][i].countB;
				    if (svScores[c1][c2] > svscore) {
					svscore = svScores[c1][c2];
				    }
				    sv_match = 1;
				    if (Math.abs(svs[chr][i].posA - oldPosA) < window / 2) {
					double valtmp = (svs[chr][i].countA + 0.5) * (svs[chr][i].countB + 0.5);
					if (valtmp > internal) { 
					    internal = valtmp;
					    posA = svs[chr][i].posA;
					}
				    }
				}
				if (svs[chr][i].chrB == chr && (Math.abs(svs[chr][i].posB - oldPosA) <= dist_thresh || (oldPosA - svs[chr][i].posB < dist && oldPosA - svs[chr][i].posB > 0)) && ((segD[min_idx - 1][1] < segD[min_idx - 2][1] && (!svs[chr][i].oriB)) || (segD[min_idx - 1][1] > segD[min_idx - 2][1] && (svs[chr][i].oriB)))) {
				    int c1 = (svs[chr][i].countA > 1) ? 2 : svs[chr][i].countA;
				    int c2 = (svs[chr][i].countB > 1) ? 2 : svs[chr][i].countB;
				    if (svScores[c1][c2] > svscore) {
					svscore = svScores[c1][c2];
				    }
				    sv_match = 1;
				    if (Math.abs(svs[chr][i].posB - oldPosA) < window / 2) {
					double valtmp = (svs[chr][i].countA + 0.5) * (svs[chr][i].countB + 0.5);
					if (valtmp > internal) { 
					    internal = valtmp;
					    posA = svs[chr][i].posB;
					}
				    }
				}
			    }
			    segI[min_idx - 1][1] = posA;
			    if (oldPosA - segI[min_idx - 2][2] < window) segI[min_idx - 2][2] = segI[min_idx - 1][1] - 1;
			    segI[min_idx - 1][4] = sv_match;
			    segD[min_idx - 1][6] = svscore;
			}
			    
			double g = (segI[min_idx - 2][0] >= snps.length && male) ? segD[min_idx - 2][2] * 2 : segD[min_idx - 2][2], 
				dmean1 = (segI[min_idx - 2][0] >= snps.length && male) ? segD[min_idx - 2][3] * 2 : segD[min_idx - 2][3];
			int idx = min_idx - 2;
			while (g > 2 && (idx - 1) > 0 && segI[idx - 1][0] == segI[min_idx - 1][0]) {
			    idx--;
			    g = (segI[idx][0] >= snps.length && male) ? segD[idx][2] * 2 : segD[idx][2];
			    dmean1 = (segI[idx][0] >= snps.length && male) ? segD[idx][3] * 2 : segD[idx][3];
			}
			double gscore1 = (g > 2) ? -1 : Math.pow((1 - Math.abs(g - 1))/0.9, gpower), 
				dmeanscore1 = (segI[idx][3] >= 10000 || Math.abs(dmean1 - 1) >= thresh_diff || (Math.abs(segD[idx][1]) < thresh_diff && Math.abs(segD[idx][4]) < thresh_log) || (segD[idx][1] * segD[idx][4] <= 0)) ? 1 : Math.pow(Math.abs(dmean1 - 1) / thresh_diff, gpower);
			if (readAI && g < 0.9 && segI[idx][0] < snps.length) {
			    int chr = segI[idx][0], posA = segI[idx][1], posB = segI[idx][2];
			    int c1 = Arrays.binarySearch(snps[chr - 1], posA), c2 = Arrays.binarySearch(snps[chr - 1], posB);
			    if (c1 < 0) c1 = -c1 - 1;
			    if (c2 < 0) c2 = -c2 - 1;
			    double g1 = (c2 - c1) * (snps[chr - 1][snps[chr - 1].length - 1] - snps[chr - 1][0]) * 1.0 /(snps[chr - 1].length * (posB - posA + 1));
			    if (g1 > gscore1) gscore1 = g1;
			}
			if (gscore1 > 1) gscore1 = 1;
			double lscore1 = (segD[idx][0] > 0.9 || (segI[idx][3] > 10000 && segD[idx][0] > 0.475) || segI[idx][3] > 100000) ? 1 : Math.pow(segD[idx][0]/0.9, lpower);
			double lscore2 = (segD[min_idx - 1][0] > 0.9 || (segI[min_idx - 1][3] > 10000 && segD[min_idx - 1][0] > 0.475) || segI[min_idx - 1][3] > 100000) ? 1 : Math.pow(segD[min_idx - 1][0]/0.9, lpower);
			double vscore1 = ((Math.abs(segD[idx][1]) < thresh_diff && Math.abs(segD[idx][4]) < thresh_log) || segD[idx][1] * segD[idx][4] > 0) ? 1 : 0;
			double vscore2 = ((segD[min_idx - 1][1] < thresh_diff && segD[min_idx - 1][4] < thresh_log) || segD[min_idx - 1][1] * segD[min_idx -1][4] > 0) ? 1 : 0;
			double init_g = g;
			g = (segI[min_idx - 1][0] >= snps.length && male) ? segD[min_idx - 1][2] * 2 : segD[min_idx - 1][2];
			double dmean2 = (segI[min_idx - 1][0] >= snps.length && male) ? segD[min_idx - 1][3] * 2 : segD[min_idx - 1][3];
			double gscore2 = (g > 2) ? 0 : Math.pow((1 - Math.abs(g - 1))/0.9, gpower), 
			       dmeanscore2 = (segI[min_idx-1][3] >= 10000 || Math.abs(dmean2 - 1) >= thresh_diff || (Math.abs(segD[min_idx - 1][1]) < thresh_diff && Math.abs(segD[min_idx - 1][4]) < thresh_log) || (segD[min_idx - 1][1] * segD[min_idx - 1][4] <= 0)) ? 1 : Math.pow(Math.abs(dmean2 - 1) / thresh_diff, gpower);
			if (readAI && g < 0.9 && (segI[min_idx - 1][0] < snps.length || (!male && segI[min_idx - 1][0] == snps.length))) {
			    int chr = segI[min_idx - 1][0], posA = segI[min_idx - 1][1], posB = segI[min_idx - 1][2];
			    int c1 = Arrays.binarySearch(snps[chr - 1], posA), c2 = Arrays.binarySearch(snps[chr - 1], posB);
			    if (c1 < 0) c1 = -c1 - 1;
			    if (c2 < 0) c2 = -c2 - 1;
			    double g1 = (c2 - c1) * (snps[chr - 1][snps[chr - 1].length - 1] - snps[chr - 1][0]) * 1.0 /(snps[chr - 1].length * (posB - posA + 1));
			    if (g1 > gscore2) gscore2 = g1;
			}
			if (gscore2 > 1) gscore2 = 1;
			double dscore = Math.abs(segD[min_idx - 1][1] - segD[idx][1]) / thresh_diff;
			if (segI[min_idx - 1][0] >= snps.length && male) dscore *= 2;
			if (dscore > 8) {
			    dscore = 2;
			} else if (dscore > 1) {
			    dscore = 1 + (dscore - 1) / 7;
			}
			double lrscore = Math.abs(segD[min_idx - 1][4] - segD[idx][4]) / thresh_log;
			if (lrscore > 6) {
			    lrscore = 2;
			} else if (lrscore > 1) {
			    lrscore = 1 + (lrscore - 1) / 5;
			}
			double tmpSVscore = segD[min_idx - 1][6];
			if (segI[min_idx - 1][3] >=10000 && segI[idx][3] >=10000 && tmpSVscore < svScores[0][1]) tmpSVscore = svScores[0][1];
			segD[min_idx - 1][5] = tmpSVscore * lscore1 * gscore1 * lscore2 * gscore2 * vscore1 * vscore2 * lrscore * dscore;
			if (segI[min_idx-1][1] - segI[idx][2] >= 100 * window) {
				segD[min_idx - 1][5] = Math.max(segD[min_idx - 1][6], svScores[0][1]) * gscore1 * gscore2 * vscore1 * vscore2 * lrscore * dscore;
			    if (debug && (debug_chr < 0 || segI[min_idx-1][0] == debug_chr)) { 
				System.out.println("Special:\t" + segI[idx][1] + " " + segI[idx][2] + " " + segD[idx][1] + " " + segD[idx][2] + " " + segD[idx][4]);
				System.out.println(segI[min_idx-1][1] + " " + segI[min_idx-1][2] + " " + segD[min_idx-1][1] + " " + segD[min_idx-1][2] + " " + segD[min_idx-1][4] + " " + segD[min_idx-1][5]);
			    }
			}
			if(checkDmeans) segD[min_idx - 1][5] *= (dmeanscore2 * dmeanscore1);
			if (segD[min_idx - 1][6] >= svScores[2][0] &&  segD[min_idx - 1][5] < segD[min_idx - 1][6] * vscore1 * vscore2 * lrscore * dscore) segD[min_idx - 1][5] = segD[min_idx - 1][6] * lrscore * dscore;
			if ((segI[min_idx - 1][3] <=50 || segI[idx][3] <=50 || (segI[min_idx - 1][3] <= 1000 && Math.abs(g - 1) > 0.2) || (segI[idx][3] <= 1000 && Math.abs(init_g - 1) > 0.2)) &&  segI[min_idx - 1][4] < 1) segD[min_idx - 1][5] /= 10;
			//if (segI[min_idx - 1][0] == 21) System.out.println(segI[min_idx-1][1] + "," + segI[min_idx-1][2] + "," + segD[min_idx-1][0] + "," + segD[min_idx-1][2] + "\t" + segD[idx][0] + "," + segD[idx][2] + "," + segD[min_idx-1][5] + "\t" + lscore1 + "," + lscore2 + "\t" + gscore1 + "," + gscore2 + "," + lrscore + "," + dscore);
		    }
		    if (min_idx > count  - 1) continue;
		    if (segI[min_idx - 1][0] != segI[min_idx][0]) {
			segD[min_idx][5] = 1;
		    } else {
			if ((segD[min_idx][1] - diff2) * (segD[min_idx][1] - segD[min_idx - 1][1]) < 0 && (!noSV)) {
			    int dist = segI[min_idx][1]-segI[min_idx-1][2];
			    int oldPosA = (int) (Math.round((segI[min_idx][1] - 1) / 100.0) * 100) + 1, posA = oldPosA, sv_match=0, chr = segI[min_idx - 1][0];
			    int num = segI[min_idx][3];
			    double dist_thresh = (num * window / 2 < distance_thresh) ? num * window / 2 : distance_thresh;
			    svscore = SV_minVal;
			    double internal = 1.5;
			    for (int i = 0; i < counts[chr]; i++) {
			    	if (svs[chr][i].chrA == chr && (Math.abs(svs[chr][i].posA - oldPosA) <= dist_thresh  || (oldPosA - svs[chr][i].posA < dist && oldPosA - svs[chr][i].posA > 0))&& 
				    (svs[chr][i].chrB != chr || Math.abs(svs[chr][i].posB - oldPosA) >= Math.abs(svs[chr][i].posA - oldPosA)) &&
				    ((segD[min_idx][1] > segD[min_idx - 1][1] && (!svs[chr][i].oriA)) || (segD[min_idx][1] < segD[min_idx - 1][1] && (svs[chr][i].oriA)))) {
				    int c1 = (svs[chr][i].countA > 1) ? 2 : svs[chr][i].countA;
				    int c2 = (svs[chr][i].countB > 1) ? 2 : svs[chr][i].countB;
				    if (svScores[c1][c2] > svscore) {
					svscore = svScores[c1][c2];
				    }
				    sv_match = 1;
				    if (Math.abs(svs[chr][i].posA - oldPosA) < window / 2) {
					double valtmp = (svs[chr][i].countA + 0.5) * (svs[chr][i].countB + 0.5);
					if (valtmp > internal) { 
					    internal = valtmp;
					    posA = svs[chr][i].posA;
					}
				    }
				}
				if (svs[chr][i].chrB == chr && (Math.abs(svs[chr][i].posB - oldPosA) <= dist_thresh || (oldPosA - svs[chr][i].posB < dist && oldPosA - svs[chr][i].posB > 0)) && 
				    (svs[chr][i].chrA != chr || Math.abs(svs[chr][i].posA - oldPosA) >= Math.abs(svs[chr][i].posB - oldPosA)) &&
				    ((segD[min_idx][1] < segD[min_idx - 1][1] && (!svs[chr][i].oriB)) || (segD[min_idx][1] > segD[min_idx - 1][1] && (svs[chr][i].oriB)))) {
				    int c1 = (svs[chr][i].countA > 1) ? 2 : svs[chr][i].countA;
				    int c2 = (svs[chr][i].countB > 1) ? 2 : svs[chr][i].countB;
				    if (svScores[c1][c2] > svscore) {
					svscore = svScores[c1][c2];
				    }
				    sv_match = 1;
				    if (Math.abs(svs[chr][i].posB - oldPosA) < window / 2) {
					double valtmp = (svs[chr][i].countA + 0.5) * (svs[chr][i].countB + 0.5);
					if (valtmp > internal) { 
					    internal = valtmp;
					    posA = svs[chr][i].posB;
					}
				    }
				}
			    }
			    segI[min_idx][1] = posA;
			    if (oldPosA - segI[min_idx - 1][2] < window) segI[min_idx - 1][2] = segI[min_idx][1] - 1;
			    segI[min_idx][4] = sv_match;
			    segD[min_idx][6] = svscore;
			}
			double g = (segI[min_idx][0] >= snps.length && male) ? segD[min_idx][2] * 2 : segD[min_idx][2],
				dmean1 = (segI[min_idx][0] >= snps.length && male) ? segD[min_idx][3] * 2 : segD[min_idx][3];
			double gscore1 = (g > 2) ? 0 : Math.pow((1 - Math.abs(g - 1))/0.9, gpower),
				dmeanscore1 = (segI[min_idx][3] >= 10000 || Math.abs(dmean1 - 1) >= thresh_diff || (Math.abs(segD[min_idx][1]) < thresh_diff && Math.abs(segD[min_idx][4]) < thresh_log) || (segD[min_idx][1] * segD[min_idx][4] <= 0)) ? 1 : Math.pow(Math.abs(dmean1 - 1) / thresh_diff, gpower);
			if (readAI && g < 0.9 && (segI[min_idx][0] < snps.length || (!male && segI[min_idx][0] == snps.length))) {
			    int chr = segI[min_idx][0], posA = segI[min_idx][1], posB = segI[min_idx][2];
			    int c1 = Arrays.binarySearch(snps[chr - 1], posA), c2 = Arrays.binarySearch(snps[chr - 1], posB);
			    if (c1 < 0) c1 = -c1 - 1;
			    if (c2 < 0) c2 = -c2 - 1;
			    double g1 = (c2 - c1) * (snps[chr - 1][snps[chr - 1].length - 1] - snps[chr - 1][0]) * 1.0 /(snps[chr - 1].length * (posB - posA + 1));
			    if (g1 > gscore1) gscore1 = g1;
			}
			if (gscore1 > 1) gscore1 = 1;
			double init_g = g;
			g = (segI[min_idx - 1][0] >= snps.length && male) ? segD[min_idx - 1][2] * 2 : segD[min_idx - 1][2];
			double dmean2 = (segI[min_idx - 1][0] >= snps.length && male) ? segD[min_idx - 1][3] * 2 : segD[min_idx - 1][3];
			int idx = min_idx - 1;
			while (g > 2 && (idx - 1) >= 0 && segI[idx - 1][0] == segI[min_idx][0]) {
			    idx--;
			    g = (segI[idx][0] >= snps.length && male) ? segD[idx][2] * 2 : segD[idx][2];
			    dmean2 = (segI[idx][0] >= snps.length && male) ? segD[idx][3] * 2 : segD[idx][3];
			}
			double gscore2 = (g > 2) ? -1 : Math.pow((1 - Math.abs(g - 1))/0.9, gpower),
				dmeanscore2 = (segI[idx][3] >= 10000 || Math.abs(dmean2 - 1) >= thresh_diff || (Math.abs(segD[idx][1]) < thresh_diff && Math.abs(segD[idx][4]) < thresh_log) || (segD[idx][1] * segD[idx][4] <= 0)) ? 1 : Math.pow(Math.abs(dmean2 - 1) / thresh_diff, gpower);
			if (readAI && g < 0.9 && segI[idx][0] < snps.length) {
			    int chr = segI[idx][0], posA = segI[idx][1], posB = segI[idx][2];
			    int c1 = Arrays.binarySearch(snps[chr - 1], posA), c2 = Arrays.binarySearch(snps[chr - 1], posB);
			    if (c1 < 0) c1 = -c1 - 1;
			    if (c2 < 0) c2 = -c2 - 1;
			    double g1 = (c2 - c1) * (snps[chr - 1][snps[chr - 1].length - 1] - snps[chr - 1][0]) * 1.0 /(snps[chr - 1].length * (posB - posA + 1));
			    if (g1 > gscore2) gscore2 = g1;
			}
			if (gscore2 > 1) gscore2 = 1;
			double lscore1 = (segD[min_idx][0] > 0.9 || (segI[min_idx][3] > 10000 && segD[min_idx][0] > 0.475) || segI[min_idx][3] > 100000) ? 1 : Math.pow(segD[min_idx][0]/0.9, lpower);
			double lscore2 = (segD[idx][0] > 0.9 || (segI[idx][3] > 10000 && segD[idx][0] > 0.475) || segI[idx][3] > 100000) ? 1 : Math.pow(segD[idx][0]/0.9, lpower);
			double vscore1 = ((Math.abs(segD[min_idx][1]) < thresh_diff && Math.abs(segD[min_idx][4]) < thresh_log) || segD[min_idx][1] * segD[min_idx][4] > 0) ? 1 : 0;
			double vscore2 = ((segD[idx][1] < thresh_diff && segD[idx][4] < thresh_log) || segD[idx][1] * segD[idx][4] > 0) ? 1 : 0;
			double dscore = Math.abs(segD[idx][1] - segD[min_idx][1]) / thresh_diff;
			if (segI[min_idx][0] >= snps.length && male) dscore *= 2;
			if (dscore > 8) {
			    dscore = 2;
			} else if (dscore > 1) {
			    dscore = 1 + (dscore - 1) / 7;
			}
			double lrscore = Math.abs(segD[idx][4] - segD[min_idx][4]) / thresh_log;
			if (lrscore > 6) {
			    lrscore = 2;
			} else if (lrscore > 1) {
			    lrscore = 1 + (lrscore - 1) / 5;
			}
			double tmpSVscore = segD[min_idx][6];
			if (segI[min_idx][3] >=10000 && segI[idx][3] >=10000 && tmpSVscore < svScores[0][1]) tmpSVscore = svScores[0][1];
			segD[min_idx][5] = tmpSVscore * lscore1 * gscore1 * lscore2 * gscore2 * vscore1 * vscore2 * lrscore * dscore;
			if (segI[min_idx][1] - segI[idx][2] >= 100 * window) {
				segD[min_idx][5] = Math.max(segD[min_idx][6], svScores[0][1]) * gscore1 * gscore2 * vscore1 * vscore2 * lrscore * dscore;
			    if (debug && (debug_chr < 0 || segI[min_idx-1][0] == debug_chr)) { 
				System.out.println("Special:\t" + segI[min_idx-1][1] + " " + segI[min_idx-1][2] + " " + segD[min_idx-1][1] + " " + segD[min_idx-1][2] + " " + segD[min_idx-1][4]);
				System.out.println(segI[min_idx][1] + " " + segI[min_idx][2] + " " + segD[min_idx][1] + " " + segD[min_idx][2] + " " + segD[min_idx][4] + " " + segD[min_idx][5]);
			    }
			}
			if (checkDmeans) segD[min_idx][5] *= (dmeanscore1 * dmeanscore2);
			if (segD[min_idx][6] >= svScores[2][0] &&  segD[min_idx][5] < segD[min_idx][6] * vscore1 * vscore2 * lrscore * dscore) segD[min_idx][5] = segD[min_idx][6] * lrscore * dscore;
			if ((segI[min_idx][3] <=50 || segI[idx][3] <=50 || (segI[min_idx - 1][3] <= 1000 && Math.abs(init_g - 1) > 0.2) || (segI[idx][3] <= 1000 && Math.abs(g - 1) > 0.2)) &&  segI[min_idx][4] < 1) segD[min_idx][5] /= 10;
			//if (segI[min_idx][0] == 21) System.out.println(segI[min_idx][1] + "," + segI[min_idx][2] + "," + segD[min_idx][0] + "," + segD[min_idx][2] + "," + segD[min_idx][5] + "\t" + lscore1 + "\t" + lscore2 + "\t" + gscore1 + "\t" + gscore2 + "\t" + lrscore + "\t" + dscore);
		    }

		} else {
		    break;
		}
	    }
	    NumberFormat formatter = new DecimalFormat("0.000");
	    for (int i = 0; i < count; i++) {
		for (int j = 0; j < 4; j++) {
		    pw.print(segI[i][j] + "\t");
		    pw2.print(segI[i][j] + "\t");
		}
		for (int j = 0; j < 6; j++) {
		    pw.print(formatter.format(segD[i][j]) + "\t");
		    pw2.print(formatter.format(segD[i][j]) + "\t");
		}
		pw.println((segI[i][4] + ((i == count - 1) ? 2 : (2*segI[i + 1][4]))));
		pw2.println((segI[i][4] + ((i == count - 1) ? 2 : (2*segI[i + 1][4]))));
	    }
	    pw.close(); pw2.close();
	    if (!noSV) {
		pw = new PrintWriter(new FileWriter(segFile + ".CNAcalls"));
		pw2 = new PrintWriter(new FileWriter(segFile + timestamp + ".CNAcalls"));
		pw.println("chrom\tloc.start\tloc.end\tnum.mark\tLog2Ratio");
		pw2.println("chrom\tloc.start\tloc.end\tnum.mark\tLog2Ratio");
		for (int i = 0; i < count; i++) {
		    if ((Math.abs(segD[i][1]) >= thresh_diff || (male && segI[i][0] >= snps.length && Math.abs(segD[i][1]) * 2 >= thresh_diff)) && Math.abs(segD[i][4]) >= thresh_log) {
			if ((male && segI[i][0] >= snps.length && Math.abs(segD[i][2] - 0.5) < 0.275) || ((segI[i][0] < snps.length || ((!male) && segI[i][0] == snps.length)) && Math.abs(segD[i][2] - 1) < 0.55)) {
			    if (segI[i][0] < snps.length) {
				pw.print(segI[i][0]);
				pw2.print(segI[i][0]);
			    } else if (segI[i][0] == snps.length) {
				pw.print("X");
				pw2.print("X");
			    } else {
				pw.print("Y");
				pw2.print("Y");
			    }
			    for (int j = 1; j < 4; j++) {
				pw.print("\t" + segI[i][j]);
				pw2.print("\t" + segI[i][j]);
			    }
			    pw.println("\t" + formatter.format(segD[i][4]));
			    pw2.println("\t" + formatter.format(segD[i][4]));
			}
		    }
		    if (i == 0 || segI[i][0] != segI[i - 1] [0]) continue;
		    double dist_thresh = (segI[i][3] * window / 2 < distance_thresh) ? segI[i][3] * window / 2 : distance_thresh;
			for (int j = 0; j < counts[segI[i][0]]; j++) {
			    if (svs[segI[i][0]][j].chrA == segI[i][0] && Math.abs(svs[segI[i][0]][j].posA - segI[i][1]) <= dist_thresh &&
				(svs[segI[i][0]][j].chrB != segI[i][0] || Math.abs(svs[segI[i][0]][j].posB - segI[i][1]) >= Math.abs(svs[segI[i][0]][j].posA - segI[i][1])) &&
				((segD[i][1] > segD[i - 1][1] && (!svs[segI[i][0]][j].oriA)) || (segD[i][1] < segD[i - 1][1] && (svs[segI[i][0]][j].oriA)))) {
				svs[segI[i][0]][j].confirmA = true;
				//System.out.println(segI[i][0] + "\t" + segI[i][1] + "\t" + segI[i][2] + "\t" + svs[segI[i][0]][j].posA);
			    }
			    if (svs[segI[i][0]][j].chrB == segI[i][0] && Math.abs(svs[segI[i][0]][j].posB - segI[i][1]) <= dist_thresh &&
				(svs[segI[i][0]][j].chrA != segI[i][0] || Math.abs(svs[segI[i][0]][j].posA - segI[i][1]) >= Math.abs(svs[segI[i][0]][j].posB - segI[i][1])) &&
				((segD[i][1] > segD[i - 1][1] && (svs[segI[i][0]][j].oriB)) || (segD[i][1] < segD[i - 1][1] && (!svs[segI[i][0]][j].oriB)))) {
				svs[segI[i][0]][j].confirmB = true;
				//System.out.println(segI[i][0] + "\t" + segI[i][1] + "\t" + segI[i][2] + "\t" + svs[segI[i][0]][j].posB + "\t" + (segD[i][1] > segD[i - 1][1]) + "\t" + svs[segI[i][0]][j].oriB + "\t" + (segD[i][1] > segD[i - 1][1] && (!svs[segI[i][0]][j].oriB)) + "\t" + (segD[i][1] < segD[i - 1][1] && (svs[segI[i][0]][j].oriB)));
			    }
			}
		}
		pw.close(); pw2.close();
		System.out.println("Writing local CREST file with quality: " + CRESTFile + ".QualityMerge");
		pw = new PrintWriter(new FileWriter(CRESTFile + ".QualityMerge"));
		pw2 = new PrintWriter(new FileWriter(CRESTFile + timestamp + ".QualityMerge"));
		for (int i = 1; i < snps.length+2; i++) {
		    for (int j = 0; j < counts[i]; j++) {
			if (!svs[i][j].checked) {
			    if (svs[i][j].confirmA || svs[i][j].confirmB) {
				pw.println(svs[i][j]);
				pw2.println(svs[i][j]);
			    }
			    svs[i][j].checked = true;
			}
		    }
		}
		pw.close(); pw2.close();
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    System.exit(-1);
	}
		    
    }



    class SV {
	int chrA, posA, chrB, posB, countA=2, countB=2;
	boolean oriA, oriB, confirmA = false, confirmB = false, checked = false;

	SV(int cA, int pA, boolean oA, int cB, int pB, boolean oB){
	    chrA = cA; posA = pA; oriA = oA; chrB = cB; posB = pB; oriB = oB;
	}
	SV(int cA, int pA, boolean oA, int countA, int cB, int pB, boolean oB, int countB){
	    this(cA, pA, oA, cB, pB, oB);
	    this.countA = countA; this.countB = countB;
	}

	boolean isSame(SV s) {
	    if ((s.chrA == chrA) && (s.posA == posA) && (s.oriA == oriA) && (s.chrB == chrB) && (s.posB == posB) && (s.oriB == oriB)) return true;
	    if ((s.chrA == chrB) && (s.posA == posB) && (s.oriA != oriB) && (s.chrB == chrA) && (s.posB == posA) && (s.oriB != oriA)) return true;
	    return false;
	}
	public String toString() {
	    String res = "" + chrA;
	    if (chrA == 0) res = "NonStandard";
	    if (chrA == 23) res = "X";
	    if (chrA == 24) res = "Y";
	    res += "\t" + posA + (oriA ? "\t+\t" : "\t-\t") + countA + "\t";
	    if (chrB == 0) {
		res += "NonStandard";
	    } else if (chrB == 23) {
		res += "X";
	    } else if (chrB == 24) {
		res += "Y";
	    } else {
		res += chrB;
	    }
	    res += "\t" + posB + (oriB ? "\t+\t" : "\t-\t") + countB;
	    return res;
	}
    }



}
