#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'bx-python==0.7.1','bed_count_by_interval.py'
__requires__ = 'bx-python==0.7.1'
import pkg_resources
pkg_resources.run_script('bx-python==0.7.1', 'bed_count_by_interval.py')
