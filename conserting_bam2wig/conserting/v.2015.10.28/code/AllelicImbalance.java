/**************************************************************************
 * Calculate the allelic imbalance for a specific sample pair or a set of
 * sample pairs. The input is a the output file of the SNPDetect with high
 * quality settings.
 *
 * Usage: java AllelicImbalance sample input_file
 * Xiang Chen 08/27/2010
 *************************************************************************/


import java.io.*;
import java.util.*;

public class AllelicImbalance{
    public static void main(String[] args) {
	double delta = 0.1;
	String dir = null;
	int start = 0, itmp = 0;
	try {
	    itmp = Integer.parseInt(args[0]);
	    if (itmp > 0 && itmp < 50) delta = itmp / 100.0;
	    start++;
	} catch (NumberFormatException nfe) {
	    if (args[start].compareTo("-d") == 0) {
		dir = args[start + 1];
		start += 2;
		if (!dir.endsWith("/")) dir += "/";
	    }
	}
	int i = start;
	    String sample = args[i];
	    String filename = args[i + 1];
	    String output = dir + "/" + sample + ".ai";
	    try {
		BufferedReader br = null;
		try {
		    br = new BufferedReader(new FileReader(filename));
	    	    System.out.println(filename);
		} catch (FileNotFoundException fnf) {
		    br = new BufferedReader(new FileReader(filename + ".original"));
		    System.out.println(filename + ".original");
		}
	    	System.out.println(output);
		PrintWriter pw = new PrintWriter(new FileWriter(output));
		String line = br.readLine();
		boolean hg19 = line.startsWith("NormalSample");
		pw.println("Chromosome\tPosition\tAI");
		while ((line = br.readLine())!= null) {
		    if (line.indexOf("SNP") >= 0) {
			StringTokenizer st = new StringTokenizer(line, "\t");
			if (hg19) {
			    st.nextToken(); st.nextToken();
			}
			st.nextToken();
			String stmp = st.nextToken().substring(3);
			//if (stmp.compareToIgnoreCase("X") == 0) stmp = "23";
			stmp += ("\t" + st.nextToken() + "\t");
			for (int j = 0; j < 16; j++) st.nextToken();
			if (hg19) for (int j = 0; j < 9; j++) st.nextToken();
			double ratioG = Double.parseDouble(st.nextToken());
			if (ratioG < 10) {
			    line = br.readLine();
			    continue;
			}
			double ratioD = Double.parseDouble(st.nextToken());
			double dtmp = Double.parseDouble(st.nextToken());
			ratioG = dtmp / (ratioG + dtmp);
			if (ratioG >= (0.5 - delta) && ratioG <= (0.5 + delta)) {
			    dtmp = Double.parseDouble(st.nextToken());
			    ratioD = dtmp / (ratioD + dtmp);
			    pw.println(stmp + Math.abs(ratioG - ratioD));
			}
		    }
		}
		br.close();
		pw.close();
	    } catch (IOException e) {
		e.printStackTrace();
		System.exit(-1);
	    }
    }
}
