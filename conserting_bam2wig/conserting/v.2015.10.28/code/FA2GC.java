/****************************************************************************
 * Count the GC and N percentage for a fixed window size
 * @auth Xiang Chen
 * @date 08/11/2010
 *
 * Usage: java FA2GC window_size input_folder output_folder
 ****************************************************************************/
import java.io.*;
import java.util.*;
import java.text.*;

public class FA2GC {
    public static void main (String[] args) {
	int size = Integer.parseInt(args[0]);
	String in = args[1];
	if (!in.endsWith("/")) in += "/";
	String out = args[2];
	if (!out.endsWith("/")) out += "/";
	DecimalFormat myFormatter = new DecimalFormat("0.000");
	try {
	    BufferedReader br = null;
	    PrintWriter pw = null;
	    for (int i = 1; i < 25; i++) {
		if (i == 23) {
//		    br = new BufferedReader(new FileReader("/nfs_exports/apps/gnu-apps/NextGen/nextgensupport/WashU_hg18_nib/X.fa"));
		    br = new BufferedReader(new FileReader(in + "X.fa"));
		    pw = new PrintWriter(new FileWriter(out + "X_" + size + ".txt"));
		} else if (i == 24) {
//		    br = new BufferedReader(new FileReader("/nfs_exports/apps/gnu-apps/NextGen/nextgensupport/WashU_hg18_nib/Y.fa"));
                    br = new BufferedReader(new FileReader(in + "Y.fa"));
		    pw = new PrintWriter(new FileWriter(out + "Y_" + size + ".txt"));
		} else {
//		    br = new BufferedReader(new FileReader("/nfs_exports/apps/gnu-apps/NextGen/nextgensupport/WashU_hg18_nib/" + i + ".fa"));
                    br = new BufferedReader(new FileReader(in + i + ".fa"));
 		    pw = new PrintWriter(new FileWriter(out + i + "_" + size + ".txt"));
		}
		pw.println("Position\tGC\tN");
		int count = 0, pos = 0, sum_gc = 0, sum_n = 0;
		br.readLine();
		String line = br.readLine();
		while (line != null) {
		    for (int j = 0; j < line.length(); j++) {
			switch (line.charAt(j)) {
			    case 'G':
			    case 'g':
			    case 'C':
			    case 'c': sum_gc++; pos++; break;
			    case 'N':
			    case 'n': sum_n++; pos++; break;
			    case 'A':
			    case 'a':
			    case 'T':
			    case 't': pos++; break;
			    default: pos++; sum_n++;
				//System.err.println(line);
				//System.exit(-1);
			}
			if (pos == size) {
			    pw.print((count * size + size / 2) + "\t");
			    if (sum_n == pos) {
				pw.println("NA\t1.0");
			    } else {
				pw.println(myFormatter.format(sum_gc * 1.0 / (pos - sum_n)) + "\t" + myFormatter.format(sum_n * 1.0 / pos));
			    }
			    count++;
			    pos = 0;
			    sum_gc = 0;
			    sum_n = 0;
			}
		    }
		    line = br.readLine();
		}
		if (pos > 0) {
		    pw.print(count * size + pos / 2); pw.print('\t');
		    if (sum_n == pos) {
			pw.println("NA\t1.0");
		    } else {
			pw.println(myFormatter.format(sum_gc * 1.0 / (pos - sum_n)) + "\t" + myFormatter.format(sum_n * 1.0 / pos));
		    }
		}
		pw.close();
		br.close();
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    System.exit(-2);
	}
    }
}
