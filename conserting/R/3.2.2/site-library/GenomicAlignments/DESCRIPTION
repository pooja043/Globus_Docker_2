Package: GenomicAlignments
Title: Representation and manipulation of short genomic alignments
Description: Provides efficient containers for storing and manipulating
	short genomic alignments (typically obtained by aligning short reads
	to a reference genome). This includes read counting, computing the
	coverage, junction detection, and working with the nucleotide content
	of the alignments.
Version: 1.4.2
Author: Herv\'e Pag\`es, Valerie Obenchain, Martin Morgan
Maintainer: Bioconductor Package Maintainer <maintainer@bioconductor.org>
biocViews: Genetics, Infrastructure, DataImport, Sequencing, RNASeq,
        SNP, Coverage, Alignment
Depends: R (>= 2.10), methods, BiocGenerics (>= 0.11.3), S4Vectors (>=
        0.5.14), IRanges (>= 2.1.26), GenomeInfoDb (>= 1.1.20),
        GenomicRanges (>= 1.19.23), Biostrings (>= 2.35.1), Rsamtools
        (>= 1.19.39)
Imports: methods, stats, BiocGenerics, S4Vectors, IRanges,
        GenomicRanges, Biostrings, Rsamtools, BiocParallel
LinkingTo: S4Vectors, IRanges
Suggests: ShortRead, rtracklayer, BSgenome, GenomicFeatures,
        RNAseqData.HNRNPC.bam.chr14, pasillaBamSubset,
        TxDb.Hsapiens.UCSC.hg19.knownGene,
        TxDb.Dmelanogaster.UCSC.dm3.ensGene,
        BSgenome.Dmelanogaster.UCSC.dm3, BSgenome.Hsapiens.UCSC.hg19,
        DESeq, edgeR, RUnit, BiocStyle
License: Artistic-2.0
Collate: utils.R cigar-utils.R GAlignments-class.R
        GAlignmentPairs-class.R GAlignmentsList-class.R
        GappedReads-class.R OverlapEncodings-class.R
        findMateAlignment.R readGAlignments.R junctions-methods.R
        sequenceLayer.R pileLettersAt.R stackStringsFromBam.R
        intra-range-methods.R coverage-methods.R setops-methods.R
        findOverlaps-methods.R mapCoords-methods.R
        coordinate-mapping-methods.R encodeOverlaps-methods.R
        findCompatibleOverlaps-methods.R summarizeOverlaps-methods.R
        findSpliceOverlaps-methods.R zzz.R
Video: https://www.youtube.com/watch?v=2KqBSbkfhRo ,
        https://www.youtube.com/watch?v=3PK_jx44QTs
NeedsCompilation: yes
Packaged: 2015-10-02 03:56:43 UTC; biocbuild
Built: R 3.2.2; x86_64-pc-linux-gnu; 2015-11-20 01:50:57 UTC; unix
