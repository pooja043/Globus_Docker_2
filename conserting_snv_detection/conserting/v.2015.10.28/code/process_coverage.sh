#!/bin/bash

INPUTBW=$1	## BigWiggle file for analysis
SAMPLE=$2	## Sample name
DEST_DIR=$3	## Destination Directory
TAG=$4		## D for tumor, G for normal, alternative tumor tags is acceptable
BIN_SIZE=$5	## window size used in CONSERTING
##Expected output: ${DEST_DIR}/${SAMPLE}_${TAG}_chr[1|2|...|23,24]_${BIN_SIZE}

if test ! -d ${DEST_DIR}
then
    mkdir ${DEST_DIR}
fi

TEMP=`mktemp`
if test ! -s ${DEST_DIR}/${SAMPLE}_${TAG}_chr24_${BIN_SIZE}
then
    if test -s ${TEMP}
    then
	rm ${TEMP}
    fi
    bigWigToWig -chrom=chr1 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr1_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr2 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr2_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr3 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr3_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr4 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr4_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr5 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr5_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr6 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr6_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr7 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr7_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr8 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr8_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr9 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr9_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr10 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr10_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr11 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr11_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr12 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr12_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr13 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr13_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr14 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr14_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr15 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr15_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr16 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr16_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr17 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr17_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr18 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr18_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr19 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr19_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr20 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr20_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr21 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr21_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chr22 ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr22_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chrX ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr23_${BIN_SIZE} -sd 3
    rm ${TEMP}

    bigWigToWig -chrom=chrY ${INPUTBW} ${TEMP}
    java -classpath /mnt/galaxyTools/tools/conserting/v.2015.10.28/code Cov2Input -f ${BIN_SIZE} -i ${TEMP} -o ${DEST_DIR}/${SAMPLE}_${TAG}_chr24_${BIN_SIZE} -sd 3
    rm ${TEMP}
fi
