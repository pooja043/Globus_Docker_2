#!/bin/sh
# Runs SAMStreamingSNPFinder for CONSERTING
#
# This variation uses paired tumor/normal bams and high_20 options.
#
# IMPORTANT: This must be run from the CONSERTING dir, containing the subdir
# "code" with all of the dependent jar files inside.
#
# $1 = NIB directory or indexed fasta file for the reference genome
# $2 = diagnosis/tumor bam
# $3 = germline/normal bam
# $4 = output file
# $5... = other arguments passed to SAMStreamingSNPFinder

# Show usage information if no parameters were sent
if [ "$#" == 0 ]; then head -n 14 $0; exit 1; fi

# Add dependencies to CLASSPATH
#CONSERTING_ROOT_DIR=`pwd`
#export CLASSPATH=$CLASSPATH:CONSERTING_ROOT_DIR/code/av.jar:$CONSERTING_ROOT_DIR/code/sam-1.27-SIGNED.jar:$CONSERTING_ROOT_DIR/code/mysql-connector-java-5.1.10-bin.jar

# Get parameters (using 1/shift method to aid in creating variants of script)
NIBORFA=$1; shift
DBAM=$1; shift
GBAM=$1; shift
OUTPUT=$1; shift
ARGS=$*

# Write diagnostics
echo $0
hostname
date

# Make sure output directory exists
OUT_DIR=`dirname $OUTPUT`
if [ "$OUT_DIR" != "" -a ! -e "$OUT_DIR" ]; then mkdir -p $OUT_DIR; fi

# Reference sequence argument
if [ -f "$NIBORFA" ]
then REFSEQARG="-fasta $NIBORFA"
else REFSEQARG="-nib $NIBORFA"
fi

# Do the run
java -Xmx15500m \
  Ace2.SAMStreamingSNPFinder \
  -bam $DBAM \
  -tn T \
  -bam $GBAM \
  -tn N \
  -of $OUTPUT \
  $REFSEQARG \
  -min-quality 20 \
  -min-flanking-quality 20 \
  -min-alt-allele-count 3 \
  -min-minor-frequency 0 \
  -broad-min-quality 10 \
  -mmf-max-hq-mismatches 4 \
  -mmf-min-quality 15 \
  -mmf-max-any-mismatches 6 \
  -unique-filter-coverage 2 \
  $ARGS
