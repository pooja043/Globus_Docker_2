import java.io.*;
import java.util.*;
import java.text.*;

public class ExtractMappability {
    public static void main(String[] args) {
	if (args.length != 4) {
	    System.err.println("Usage:");
	    System.err.println("java ExtractMappability DestDir ExtractedWigFile chrom_length_file bin_size");
	    System.exit(-1);
	}
	int size = Integer.parseInt(args[3]);
	String destDir = args[0];
	if (!destDir.endsWith("/")) destDir += "/";
	try {
	    BufferedReader br = new BufferedReader(new FileReader(args[2]));
	    int[] chr_len = new int[24];
	    String line = null;
	    StringTokenizer st = null;
	    int chr = -1;
	    while ((line = br.readLine()) != null) {
		st = new StringTokenizer(line);
		String schr = st.nextToken();
		if (schr.startsWith("chr") || schr.startsWith("Chr")) schr = schr.substring(3);
		if (schr.compareTo("X") == 0) {
		    chr = 22;
		} else if (schr.compareTo("Y") == 0) {
		    chr = 23;
		} else {
		    try {
			chr = Integer.parseInt(schr) - 1;
		    } catch (NumberFormatException e) {
			chr = -1;
		    }
		}
		if (chr >= 0) chr_len[chr] = Integer.parseInt(st.nextToken());
	    }
	    //for (int i = 0; i < 24; i++) System.out.println((i + 1) + "\t" + chr_len[i]);
	    br.close();

	    br = new BufferedReader(new FileReader(args[1]));
	    String current_chr = null;
	    PrintWriter pw = null;
	    double[] map = null;
	    NumberFormat formatter = new DecimalFormat("0.00");
	    int line_count = 0;
	    while ((line = br.readLine()) != null) {
		if (line.startsWith("#")) continue;
		st = new StringTokenizer(line);
		String schr = st.nextToken();
		if (current_chr != null && (schr.compareTo(current_chr) != 0) && chr >= 0) {
		    int count = 0;
		    double sum = 0;
		    for (int i = 0; i < map.length; i++) {
			sum += map[i];
			count++;
			if (count == size) {
			    pw.println(formatter.format(sum / size));
			    sum = 0;
			    count = 0;
			}
		    }
		    if (count > 0) pw.println(formatter.format(sum / count));
		    pw.close();
		}
		if (current_chr == null || (schr.compareTo(current_chr) != 0)) {
		    current_chr = schr;
		    if (schr.startsWith("chr"))schr = schr.substring(3);
		    if (schr.compareTo("X") == 0) {
			chr = 22;
		    } else if (schr.compareTo("Y") == 0) {
			chr = 23;
		    } else {
			try {
			    chr = Integer.parseInt(schr) - 1;
			} catch (NumberFormatException e) {
			    chr = -1;
			    continue;
			}
		    }
		    pw = new PrintWriter(new FileWriter(destDir + "Mapability_chr" + (chr + 1) + "_" + size));
		    map = new double[chr_len[chr]];
		    System.gc();
		    System.out.println(current_chr + "\t" + chr_len[chr]);
		}
		line_count++;
		//if (line_count % 10000 == 0) System.out.println(line);
		int loc1 = Integer.parseInt(st.nextToken()), loc2 = Integer.parseInt(st.nextToken());
		double val = Double.parseDouble(st.nextToken());
		for (int i = loc1; i < loc2; i++) map[i] = val;
	    }
		if (current_chr != null && chr >= 0) {
		    int count = 0;
		    double sum = 0;
		    for (int i = 0; i < map.length; i++) {
			sum += map[i];
			count++;
			if (count == size) {
			    pw.println(formatter.format(sum / size));
			    sum = 0;
			    count = 0;
			}
		    }
		    if (count > 0) pw.println(formatter.format(sum / count));
		    pw.close();
		}

	} catch (IOException ie) {
	    ie.printStackTrace();
	    System.exit(-1);
	}
    }
}
