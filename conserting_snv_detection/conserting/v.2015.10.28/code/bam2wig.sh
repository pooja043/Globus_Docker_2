#!/bin/sh

BAM_FILE=$1      ## bam file for data analysis
SAMPLE_NAME=$2   ## the sample name associated with the bam file
OUTPUT_DIR=$3    ## the output directory for coverage wiggle files
## output file will be named as ${OUTPUT_DIR}/${SAMPLE_NAME}.wig.gz

if [ ! -d "$OUTPUT_DIR" ]; then mkdir -p "$OUTPUT_DIR"; fi
# change to the root directory where CONSERTING is installed
#CONSERTING_ROOT_DIR=`pwd` 

#export CLASSPATH=${CONSERTING_ROOT_DIR}/code/av.jar:${CONSERTING_ROOT_DIR}/code/sam-1.27-SIGNED.jar:${CONSERTING_ROOT_DIR}/code/mysql-connector-java-5.1.10-bin.jar

if test ! -s ${BAM_FILE}
then
  echo "Fail to open bam file ${BAM_FILE}"
  exit 1
fi

if test ! -s ${BAM_FILE}.bai
then
  echo "Fail to find the index file  ${BAM_FILE}.bai"
  exit 1
fi

java -Xmx8192m Ace2.SAMFinneyCoverage -bam ${BAM_FILE} -wig -stdout | \
gzip -1 >${OUTPUT_DIR}/${SAMPLE_NAME}


